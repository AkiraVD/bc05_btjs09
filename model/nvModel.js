function NhanVien(
  taiKhoan,
  tenNV,
  email,
  matKhau,
  ngayLam,
  luongCB,
  chucVu,
  gioLam
) {
  this.tk = taiKhoan;
  this.ten = tenNV;
  this.email = email;
  this.pass = matKhau;
  this.ngay = ngayLam;
  this.luong = luongCB;
  this.cv = chucVu;
  this.gio = gioLam;
  this.tongLuong = function () {
    switch (this.cv) {
      case "Sếp":
        return this.luong * 3;
      case "Trưởng phòng":
        return this.luong * 2;
      default:
        return this.luong;
    }
  };
  this.xeploai = function () {
    if (this.gio >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.gio >= 176) {
      return "Nhân viên giỏi";
    } else if (this.gio >= 160) {
      return "Nhân viên khá";
    } else return "Nhân viên trung bình";
  };
}

const formatter = new Intl.NumberFormat("vn-VN", {
  style: "currency",
  currency: "VND",
});
