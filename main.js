var dsnv = [];
const DSNV = "DSNV";

//Lưu vào storage
function saveLocalStorage() {
  let jsonDSNV = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, jsonDSNV);
}

//Load data khi tải trang
var dataJSON = localStorage.getItem(DSNV);
if (dataJSON !== null) {
  var tempArr = JSON.parse(dataJSON);

  // Convert data
  tempArr.forEach(function (item) {
    var nv = new NhanVien(
      item.tk,
      item.ten,
      item.email,
      item.pass,
      item.ngay,
      item.luong,
      item.cv,
      item.gio
    );
    dsnv.push(nv);
  });
  showThongTinLenTable(dsnv);
}

function themNV() {
  var nv = layThongTinTuForm();

  var isValid = kiemTraHopLe(nv, true);
  // Add vào danh sách nếu thông tin đúng
  if (isValid) {
    dsnv.push(nv);

    saveLocalStorage();

    showThongTinLenTable(dsnv);
  }
}

function xoaNV(taiKhoan) {
  var viTri = timViTri(taiKhoan, dsnv);

  if (viTri !== -1) {
    dsnv.splice(viTri, 1);
  }

  saveLocalStorage();

  showThongTinLenTable(dsnv);
}

function suaNV(taiKhoan) {
  var viTri = timViTri(taiKhoan, dsnv);
  showThongTinLenForm(viTri, dsnv);

  document.getElementById("tknv").disabled = true;
  document.getElementById("btnThemNV").disabled = true;
  document.getElementById("btnCapNhat").disabled = false;

  let thongBao = document.querySelectorAll(".sp-thongbao");
  thongBao.forEach(function (item) {
    item.style.display = "none";
  });
}

function capNhatNV() {
  var nvCapNhat = layThongTinTuForm();
  var viTri = timViTri(nvCapNhat.tk, dsnv);

  if (viTri == -1) {
    alert("Không tìm thấy tài khoản này");
    return;
  }

  dsnv[viTri] = nvCapNhat;

  var isValid = kiemTraHopLe(nvCapNhat, false);

  if (isValid) {
    saveLocalStorage();

    showThongTinLenTable(dsnv);
  }
}

// Add enter vào search
document
  .getElementById("searchName")
  .addEventListener("keypress", function (event) {
    // If the user presses the "Enter" key on the keyboard
    if (event.key === "Enter") {
      // Trigger the button element with a click
      document.getElementById("btnTimNV").click();
    }
  });

function search() {
  var input = document.getElementById("searchName").value;
  // console.log("input: ", input);
  var dsSearch = [];

  if (input == "") return showThongTinLenTable(dsnv);

  var regexXuatSac = /sac|sắc|sca|sx|xs|saw/iu; //
  var regexGioi = /gioi|giỏi/iu;
  var regexKha = /kha|khá/iu;
  var regexTrungBinh = /bình|binh|tb|bifn/iu;
  if (regexXuatSac.test(input)) {
    dsnv.forEach(function (item, index) {
      if (item.xeploai() == "Nhân viên xuất sắc") {
        dsSearch.push(dsnv[index]);
      }
    });
  } else if (regexGioi.test(input)) {
    dsnv.forEach(function (item, index) {
      if (item.xeploai() == "Nhân viên giỏi") {
        dsSearch.push(dsnv[index]);
      }
    });
  } else if (regexKha.test(input)) {
    dsnv.forEach(function (item, index) {
      if (item.xeploai() == "Nhân viên khá") {
        dsSearch.push(dsnv[index]);
      }
    });
  } else if (regexTrungBinh.test(input)) {
    dsnv.forEach(function (item, index) {
      if (item.xeploai() == "Nhân viên trung bình") {
        dsSearch.push(dsnv[index]);
      }
    });
  }
  // console.log("dsSearch: ", dsSearch);
  showThongTinLenTable(dsSearch);
}

function closeModal() {
  document.querySelector("#formNV").reset();

  document.getElementById("tknv").disabled = false;
  document.getElementById("btnThemNV").disabled = false;
  document.getElementById("btnCapNhat").disabled = false;
  let thongBao = document.querySelectorAll(".sp-thongbao");
  thongBao.forEach(function (item) {
    item.style.display = "none";
  });
}

function openModal() {
  document.querySelector("#formNV").reset();

  document.getElementById("tknv").disabled = false;
  document.getElementById("btnThemNV").disabled = false;
  document.getElementById("btnCapNhat").disabled = true;
  let thongBao = document.querySelectorAll(".sp-thongbao");
  thongBao.forEach(function (item) {
    item.style.display = "none";
  });
}
